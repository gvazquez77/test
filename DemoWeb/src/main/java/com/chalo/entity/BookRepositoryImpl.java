package com.chalo.entity;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

@Repository
public class BookRepositoryImpl implements BookRepository{
	EntityManager em;
	
	public BookRepositoryImpl(EntityManager em) {
		this.em=em;
	}
	
	
	@Override
	public List<Book> retriveAll(String tittle ) {
	
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Book> criteriaQuery = criteriaBuilder.createQuery(Book.class);
		
		Root<Book> root =criteriaQuery.from(Book.class);
		Predicate authorNamePredicate =criteriaBuilder.equal(root.get(Book_.TITLE), tittle);
		
		criteriaQuery
			.select(root)
			.where(authorNamePredicate);
		TypedQuery<Book> query= em.createQuery(criteriaQuery);
		return query.getResultList();
	}


	


	@Override
	public void UpdateBook(Book book) {
		em.merge(book);
		
		/*	
		CriteriaBuilder criteriaBuilder =em.getCriteriaBuilder();
		CriteriaUpdate <Book>   update = criteriaBuilder.createCriteriaUpdate(Book.class);
		
		Root root =update.from(Book.class);
		
		update.set();
		update.where( criteriaBuilder.equal(root.get(updateTitle.getTitle()), title));
				//book repository <- serivice parametro obteer un ook crear una funcion que traiga book en repo y service set a objeo otro repository 1getid 
		Query query = em.createQuery(update);
		query.executeUpdate();
		
		
		*/
		
		
	}
	
	public Book findId( int id ) {
		
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Book> criteriaQuery = criteriaBuilder.createQuery(Book.class);
		
		Root<Book> root =criteriaQuery.from(Book.class);
		Predicate IDPredicate =criteriaBuilder.equal(root.get(Book_.ID), id);
		
		criteriaQuery
		.select(root)
		.where(IDPredicate);
	TypedQuery<Book> query= em.createQuery(criteriaQuery);
	return query.getSingleResult();
		
		 
	}


	@Override
	public void insert(Book book) {
		
		em.persist(book);
	}


	@Override
	public void DeleteBook(Book book) {
		em.remove(book);
		
	}


	


	
}
