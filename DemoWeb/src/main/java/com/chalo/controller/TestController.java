package com.chalo.controller;



import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.chalo.Datos;
import com.chalo.entity.Book;
import com.chalo.entity.BookUpdateImp;
import com.chalo.services.AdditionService;
import com.chalo.services.BookServices;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/")
@AllArgsConstructor
public class TestController {
	
	private final AdditionService additionService;
	private final BookServices bookServices;
	private final BookUpdateImp bookUpdate;
	 
	@GetMapping()
	public ResponseEntity<String> getHola() {
		return ResponseEntity.ok("Hola MUNDO");
	}
	
	
	
	@PostMapping(consumes =MediaType.APPLICATION_JSON_UTF8_VALUE)
	public  ResponseEntity<Integer> getSuma(@RequestBody Datos datos){
		
		return ResponseEntity.ok(additionService.addition(datos));
	}
	
	//query params /lis?param1=abc&param2=jsdk
	//llamar a book services
	
	@GetMapping("/libro")
	public ResponseEntity <List<Book>> retribebook(@RequestParam final String tittle){
		
		return ResponseEntity.ok(bookServices.retribebook(tittle));
		
	}
	
	@PutMapping("/actualizar")
	
	public ResponseEntity<Void> updateBook(@RequestParam int id, @RequestParam String title ){
	 
		
		bookServices.UpdateBook(id, title);
		return ResponseEntity.accepted().build();
	}
	
	@PostMapping(consumes =MediaType.APPLICATION_JSON_UTF8_VALUE, path = "/insertar")
	public ResponseEntity<Void> Insert(@RequestBody Book book){
		bookServices.insert(book);
		
		return ResponseEntity.accepted().build();
		
	}
	
	@DeleteMapping(path = "/delete")
	public ResponseEntity<Void> DeleteBook(@RequestParam int id){
		bookServices.DelateBook(id);
		return ResponseEntity.accepted().build();
		
	}
	
	@GetMapping("/consulta")
	public ResponseEntity<Book> findId(@RequestParam int id){
		
		return ResponseEntity.ok(bookServices.findId(id));
	}
}
