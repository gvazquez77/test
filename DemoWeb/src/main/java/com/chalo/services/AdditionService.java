package com.chalo.services;

import org.springframework.stereotype.Service;

import com.chalo.Datos;
@Service
public class AdditionService {
	
	public int addition (Datos datos) {
		return datos.getN1()+datos.getN2();
	}
}
