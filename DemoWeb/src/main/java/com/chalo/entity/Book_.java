package com.chalo.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")

@StaticMetamodel(Book.class)
public abstract class Book_ {
public static volatile SingularAttribute<Book, Integer> id;
public static volatile SingularAttribute<Book, String> description;
public static volatile SingularAttribute<Book, String> tittle;


public static final String ID="id";
public static final String DESCRIPTION ="";
public static final String TITTLE="tittle";

}
