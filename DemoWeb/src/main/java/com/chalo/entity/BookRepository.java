package com.chalo.entity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
public interface BookRepository{
 
	 List<Book> retriveAll(String tittle);
	
	 void UpdateBook(Book id);
	 
	 void DeleteBook(Book book);
	 
	 Book findId(int id);
	 
	 void insert(Book book);
	 //delete devolver em por id y deuvlve 1 
}
